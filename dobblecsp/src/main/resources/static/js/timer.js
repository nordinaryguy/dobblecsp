$(function() {
	// Ici, le DOM est entièrement défini
	var progressBar = $('#progress-bar'), width = 0, timeleft = 0;
	var percent = $('#percent');

	progressBar.width(width);

	var interval = setInterval(function() {
		width += 10;
		progressBar.css('width', width + '%');
		timeleft = (100 - width * 1)/10;
		percent.html(timeleft + 's left !');
		if (width >= 100) {
			clearInterval(interval);
			failed();
		}
	}, 1000);

	function failed(){
		window.location.replace("./failed");
	}

	$('img').on('click', function () {
		if ($('#conteneurCarte').find('#'+this.id).length) {
			window.location.replace("/?id="+this.id);
		}else{
			tempAlert("Nope !",1000);
		}
	});

	function tempAlert(msg,duration){
		var el = document.createElement("div");
		el.innerHTML = msg;
		el.setAttribute("style","display: inline-block; position: fixed; top: 0; bottom: 0; left: 0; right: 0; border-radius: 100px;border: 2px solid black; width: 200px; height: 100px; margin: auto; background-color: #ff5000;text-align:center; color: white;font-size:2em;");
		setTimeout(function(){
			el.parentNode.removeChild(el);
		},duration);
		document.body.appendChild(el);
	}
});