package fr.univangers.dobblecsp.tests;

import org.apache.tomcat.util.digester.ArrayStack;

import fr.univangers.dobblecsp.models.Carte;

public class CarteTest {

	public static ArrayStack<Carte> test() {

		ArrayStack<Carte> cartes = new ArrayStack<>();
		for (int i = 0; i < 4; i++) {
			Carte c = new Carte(i+1);
			c.setSymboles(SymboleTest.test());
			cartes.push(c);
		}
		
		return cartes;
	}
}
