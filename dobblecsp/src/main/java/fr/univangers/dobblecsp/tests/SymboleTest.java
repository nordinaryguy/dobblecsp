package fr.univangers.dobblecsp.tests;

import java.util.ArrayList;

import fr.univangers.dobblecsp.models.Symbole;

public class SymboleTest {

	public static ArrayList<Symbole> test() {

		ArrayList<Symbole> symboles = new ArrayList<Symbole>();
		ArrayList<Integer> integerList = new ArrayList<>();

		for (int i = 0; i < 5; i++) {
			int n = randomSymb();
			if(i==0) {
				symboles.add(new Symbole(n));
				integerList.add(n);
			}
			else {
				while(integerList.contains(n)) {
					n = randomSymb();
				}
				symboles.add(new Symbole(n)) ;
				integerList.add(n);
			}
		}
		return symboles;
	}

	private static int randomSymb() {
		int high = 10;
		int low = 1;
		int n = (int)(Math.random() * (high+1-low)) + low;

		return n;
	}

}
