package fr.univangers.dobblecsp.models;

public class Symbole {
	
	private int numero; //Numéro du symbole
	private int taille; // En pourcentage ? Pour les variantes.
	private String imageUrl; 
	
	public Symbole(int numero) {
		this.numero = numero;
		this.taille = (int)Math.floor(Math.random() * 100) + 1;
		this.imageUrl = "/img/" + numero + ".png";
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "Symbole [numero=" + numero + ", taille=" + taille + ", imageUrl=" + imageUrl + "]";
	}
	
}
