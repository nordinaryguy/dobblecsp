package fr.univangers.dobblecsp.models;

import java.util.ArrayList;

public class Carte {
	
	private int numero;
	private ArrayList<Symbole> symboles = new ArrayList<Symbole>();
	
	public Carte(int numero) {
		this.numero = numero;
	}
	
	public Carte (int numero, ArrayList<Symbole> symboles) {
		this.numero = numero;
		this.symboles = symboles;
	}
	
	@Override
	public String toString() {
		return "Carte [numero=" + numero + ", symboles=" + symboles + "]";
	}

	public void addSymbole(Symbole symbole) {
		this.symboles.add(symbole);
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public ArrayList<Symbole> getSymboles() {
		return symboles;
	}

	public void setSymboles(ArrayList<Symbole> symboles) {
		this.symboles = symboles;
	}
	
}
