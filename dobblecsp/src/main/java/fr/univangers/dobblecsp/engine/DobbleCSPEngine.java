package fr.univangers.dobblecsp.engine;

import org.apache.tomcat.util.digester.ArrayStack;

import fr.univangers.dobblecsp.models.Carte;
import fr.univangers.dobblecsp.models.Symbole;

public class DobbleCSPEngine {

	private ArrayStack<Carte> cartes = new ArrayStack<>();
	private Carte carteJoueur;
	private int idOnClickSymbole;
	
	public void init(ArrayStack<Carte> cartes, Carte carteJoueur) {
		this.cartes = cartes;
		this.carteJoueur = carteJoueur;
	}
	
	public boolean isInit() {
		boolean isInit = false;
		if (!this.cartes.isEmpty() && this.carteJoueur!= null) {
			isInit = true;
		}
		return isInit;
	}
	
	public boolean process(String idSymbole) {
		boolean gagne = false;
		this.idOnClickSymbole = Integer.parseInt(idSymbole);

		Carte carteSurLaPile = this.cartes.peek();
		for (Symbole symboleCartePile : carteSurLaPile.getSymboles()) {
			if (symboleCartePile.getNumero()==this.idOnClickSymbole) {
				gagne = true;
				this.carteJoueur = this.cartes.pop();
			}
		}
		return gagne;
	}
	
	public boolean jeuTermine() {
		boolean jeuTermine = false;
		
		if (this.cartes.size() == 0) {
			jeuTermine = true;
		}
		
		return jeuTermine;
	}
	
	public Carte getCarteJoueur() {
		return this.carteJoueur;
	}
	
	public ArrayStack<Carte> getPileDeCartes(){
		return this.cartes;
	}
}
