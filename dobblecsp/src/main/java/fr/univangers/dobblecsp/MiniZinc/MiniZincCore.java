package fr.univangers.dobblecsp.MiniZinc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.tomcat.util.digester.ArrayStack;
import org.springframework.stereotype.Component;

import fr.univangers.dobblecsp.models.Carte;
import fr.univangers.dobblecsp.models.Symbole;

@Component
public class MiniZincCore {

	public static String minizincApp = "/Applications/MiniZincIDE.app/Contents/Resources/minizinc";
	public static String minizincModel = "/Users/guillaumehuet/Documents/springtoolsuiteworkspace/dobblecsp/dobblecsp/src/main/resources/static/mininzinc/dobble.mzn";

	public ArrayStack<Carte> process(int nbCartes, int nbSymboles, int nbSymbParCartes) {
		ArrayList<String> outputLines =  new ArrayList<>();
		ArrayStack<Carte> cartes = null;

		try {
			Process proc = Runtime.getRuntime().exec(minizincApp+" "+minizincModel+" -D NC="+nbCartes+";NS="+nbSymboles+";NSC="+nbSymbParCartes+";");

			try {
				if(!proc.waitFor(10, TimeUnit.SECONDS)) {
				    //timeout - kill the process. 
				    proc.destroy(); // consider using destroyForcibly instead
				    return null;
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
			BufferedReader stdInput = new BufferedReader(new
					InputStreamReader(proc.getInputStream()));

//			BufferedReader stdError = new BufferedReader(new
//					InputStreamReader(proc.getErrorStream()));

			// read the output from the command
			System.out.println("Here is the standard output of the command:\n");
			String s;
			while ((s = stdInput.readLine()) != null) {
				if (s.equals("=====UNSATISFIABLE=====") || s.equals("=====ERROR=====")) {
					return null;
				}
				System.out.println(s);
				outputLines.add(s);
			}

			// read any errors from the attempted command
			//			System.out.println("Here is the standard error of the command (if any):\n");
			//			while ((s = stdError.readLine()) != null) {
			//				System.out.println(s);
			//			}

			// Conversion de la sortie de minizinc en cartes de jeu dobble
			cartes = processOutPutLines(outputLines);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cartes;
	}

	private ArrayStack<Carte> processOutPutLines(ArrayList<String> outputLines) {
		ArrayStack<Carte> cartes = new ArrayStack<>();
		outputLines.remove(outputLines.size()-1);
		for (int i = 0; i < outputLines.size(); i++) {
			Carte c = new Carte(i+1);
			String[] datas = outputLines.get(i).split("\t");
			for (int j = 1; j < datas.length; j++) {
				int symbNumber = Integer.parseInt(datas[j].strip());
				c.addSymbole(new Symbole(symbNumber));
			}
			cartes.push(c);
		}
		return cartes;
	}
}