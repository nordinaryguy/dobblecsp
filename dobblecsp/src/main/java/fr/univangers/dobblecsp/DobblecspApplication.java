package fr.univangers.dobblecsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DobblecspApplication {

	public static void main(String[] args) {
		SpringApplication.run(DobblecspApplication.class, args);
	}

}

