package fr.univangers.dobblecsp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.digester.ArrayStack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.univangers.dobblecsp.MiniZinc.MiniZincCore;
import fr.univangers.dobblecsp.engine.DobbleCSPEngine;
import fr.univangers.dobblecsp.models.Carte;
import fr.univangers.dobblecsp.tests.CarteTest;
import fr.univangers.dobblecsp.tests.SymboleTest;

@Controller
public class MainController {

	private DobbleCSPEngine engine = new DobbleCSPEngine();

	@Autowired
	MiniZincCore minizincore;

	@GetMapping("/newGame")
	public String newGameGET(HttpSession session) {
		return "newGame/index";
	}

	@PostMapping("/newGame")
	public String newGame(HttpSession session, HttpServletRequest request) {
		int nbCartes =  Integer.valueOf(request.getParameter("nbcartes"));
		int nbSymboles =  Integer.valueOf(request.getParameter("nbsymboles"));
		int nbSymbParCartes =  Integer.valueOf(request.getParameter("nbsymbparcartes"));
		System.out.println("Nombe de cartes récupérées : " + nbCartes);
		System.out.println("Nombe de symbobles récupérés : " + nbSymboles);
		System.out.println("Nombe de symboles par carte récupérés : " + nbSymbParCartes);

		// Process with miniZinc
		ArrayStack<Carte> cartes = minizincore.process(nbCartes, nbSymboles, nbSymbParCartes);
		if ( cartes == null) {
			return "erreur/erreur";
		}else {
			Carte firstCarte = cartes.pop();
			Carte carteSurLaPile = cartes.peek();
			session.setAttribute("cartePile",carteSurLaPile);
			session.setAttribute("carteJoueur", firstCarte);
			engine.init(cartes, firstCarte);
		}
		return "redirect:/";
	}

	@GetMapping("/")
	public String home(HttpSession session,@RequestParam(value="id", required=false) String idSymb) {
		if (session.getAttribute("cartePile") == null && session.getAttribute("carteJoueur") == null) {
			return "redirect:/newGame";
		}
		boolean gagne = false;
		if (idSymb!=null) { 
			if (engine.isInit()) { 
				gagne =  engine.process(idSymb);
				if (gagne) { 
					if (engine.jeuTermine()) {
						session.invalidate();
						return "fin/fin";
					}
					session.setAttribute("cartePile", engine.getPileDeCartes().peek());
					session.setAttribute("carteJoueur", engine.getCarteJoueur());

					return "redirect:/"; 
				}
			} 
		}
		return "index";
	}

	@GetMapping("/generate")
	public String generate(HttpSession session) {
		ArrayStack<Carte> cartes = CarteTest.test();
		Carte firstCarte = cartes.pop();
		Carte carteSurLaPile = cartes.peek();
		session.setAttribute("cartePile",carteSurLaPile);
		session.setAttribute("carteJoueur", firstCarte);
		engine.init(cartes, firstCarte);
		return "redirect:/";
	}

	@GetMapping("/reset")
	public String reset(HttpSession session) {
		session.invalidate();
		return "reset/reset";
	}

	@GetMapping("/failed")
	public String failed(HttpSession session) {
		session.invalidate();

		return "fin/finFailed";
	}


	//-------------- TESTS ---------------------------//

	@GetMapping("/symboletest")
	public String symboleTest(Model model) {
		model.addAttribute("results", SymboleTest.test());
		return "symbole/index";
	}

	@GetMapping("/cartetest")
	public String carteTest(Model model) {
		model.addAttribute("results", CarteTest.test());
		return "carte/index";
	}
}
