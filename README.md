# DobbleCSP

DobbleCSP project

Pour lancer le projet, il faut : 
- Java SE 11 (JDK11) 
- Eclipse J2EE ou STS 4 (framework SpringBoot utilisé)
- Dans la classe MiniZincCore.java : 
    - Remplacer le chemin de l'application MiniZinc (ligne de commande) au niveau de la variable '***minizincApp***'
    - Remplacer le chemin du modèle au niveau de la variable '***minizincModel***'
    - Aperçu en image : 
![alt MiniZincPath](./MiniZincPath.png)

